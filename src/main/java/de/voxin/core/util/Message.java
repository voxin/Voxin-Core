package de.voxin.core.util;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

import de.voxin.core.main.CorePlugin;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class Message{
	
	private @Getter String name;
	private @Getter String content;
	private @Getter String language;
	
	public Message replaceArgs(String...strings){
		
		String replacedContent = content;
		
		for(int i = 0; i < strings.length; i++){
			replacedContent = replacedContent.replace("{" + i + "}", strings[i]);
		}
		
		return new Message(name, replacedContent, language);
	}
	
	public void broadcast() {
		CorePlugin.getInstance().getConnectPlugin().getConnectManager().sendBroadcast(content, "");
	}

	public void broadcast(String permission) {
		CorePlugin.getInstance().getConnectPlugin().getConnectManager().sendBroadcast(content, permission);
	}

	public void sendMessage(String name) {
		CorePlugin.getInstance().getConnectPlugin().getConnectManager().sendMessage(name, content);
	}
	
	public void sendMessage(CommandSender sender) {
		sender.sendMessage(content);
	}
	
	public void sendMessage(UUID uuid) {
		if(Bukkit.getPlayer(uuid) != null)
		Bukkit.getPlayer(uuid).sendMessage(content);
	}
	
	@Override
	public String toString() {
		return content;
	}
}