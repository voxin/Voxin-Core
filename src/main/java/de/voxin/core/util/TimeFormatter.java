package de.voxin.core.util;

import de.voxin.core.manager.Localization;

public class TimeFormatter {
	
	private static final int SECOND = 1000;
	private static final int MINUTE = SECOND * 60;
	private static final int HOUR = MINUTE * 60;
	private static final int DAY = HOUR * 24;
	private static final int MONTH = DAY * 30;
	
	public static String getTageStundenSekunden(long time, String language){
		String timeString = new String();
		
		int days    = (int)(time / DAY);
		int hours   = (int)((time % DAY) / HOUR);
		int minutes = (int)((time % HOUR) / MINUTE);
		int seconds = (int)((time % MINUTE) / SECOND);
		
		if(days != 0){
			timeString += days + " " + (days < 2 ? Localization.getMessage("TIME_DAY", "en_US").getContent() : Localization.getMessage("TIME_DAYS", "en_US").getContent()) + " ";
		}
		
		if(hours != 0){
			timeString += hours + " " + (hours < 2 ? Localization.getMessage("TIME_HOUR", "en_US").getContent() : Localization.getMessage("TIME_HOURS", "en_US").getContent()) + " ";
		}
		
		if(minutes != 0){
			timeString += minutes + " " + (minutes < 2 ? Localization.getMessage("TIME_MINUTE", "en_US").getContent() : Localization.getMessage("TIME_MINUTES", "en_US").getContent()) + " ";
		}
		
		if(seconds != 0){
			timeString += seconds + " " + (seconds < 2 ? Localization.getMessage("TIME_SECOND", "en_US").getContent() : Localization.getMessage("TIME_SECONDS", "en_US").getContent()) + " ";
		}
		
		return timeString;
	}
	
	public static long getTimeFromString(String string){
		long time = 0;
		
		try {
			if(string.indexOf('m') != -1){
				int m = Integer.parseInt(string.substring(0, string.indexOf('m')));
				time += m*MONTH;
			} else
			if(string.indexOf('d') != -1){
				int d = Integer.parseInt(string.substring(0, string.indexOf('d')));
				time += d*DAY;
			} else
			if(string.indexOf('h') != -1){
				int h = Integer.parseInt(string.substring(0, string.indexOf('h')));
				time += h*HOUR;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return time;
	}

}
