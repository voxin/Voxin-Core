package de.voxin.core.util;

import java.lang.reflect.InvocationTargetException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedChatComponent;

import de.voxin.core.main.CorePlugin;

public class PacketSending {
	
	private CorePlugin plugin;
	
	public PacketSending(CorePlugin plugin) {
		this.plugin = plugin;
	}
	
	public void sendHeaderFooter(String header, String footer){
		PacketContainer hfPacket = new PacketContainer(PacketType.Play.Server.PLAYER_LIST_HEADER_FOOTER);
		hfPacket.getChatComponents()
		.write(0, WrappedChatComponent.fromText(header))
		.write(1, WrappedChatComponent.fromText(footer));
		
		try {
			for(Player player : Bukkit.getOnlinePlayers()){
				plugin.getProtocolManager().sendServerPacket(player, hfPacket);
			}
		} catch (InvocationTargetException e) {
			throw new RuntimeException("Cannot send Packet" + hfPacket, e);
		}
	}

}
