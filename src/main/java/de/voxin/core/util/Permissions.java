package de.voxin.core.util;

import lombok.Getter;

public enum Permissions {
	
	PRIVATE_MESSAGING("voxin.chat.private"),
	PRIVATE_MESSAGING_COLOR("voxin.chat.private.color"),
	TELEPORT_SERVER("voxin.teleport.use"),
	PLAYER_INFO("voxin.player.info.use"),
	PLAYER_INFO_ADMIN("voxin.player.info.admin"),
	REPORT_USE("voxin.chat.report.use"),
	REPORT_SEE("voxin.chat.report.see"),
	PICKUP_LOBBY("voxin.lobby.pickup"),
	BUILD_LOBBY("voxin.lobby.build"),
	USE_MOBMENU("voxin.menu.mob"),
	ALL_COMMAND("voxin.commands.forbidden.use"),
	COLOR_PREMIUM("voxin.tablist.color.premium"),
	COLOR_MOD("voxin.tablist.color.mod"),
	COLOR_ADMIN("voxin.tablist.color.admin"),
	TEMPBAN("voxin.admin.tempban"),
	PERMBAN("voxin.admin.permban"),
	UNBAN("voxin.admin.unban");
	
	private @Getter String permission;
	
	private Permissions(String permission) {
		this.permission = permission;
	}

}
