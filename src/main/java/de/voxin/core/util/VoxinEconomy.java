package de.voxin.core.util;

import java.util.List;

import org.bukkit.OfflinePlayer;

import com.google.common.collect.Lists;

import de.voxin.connect.util.ServerType.VoxinServer;
import de.voxin.core.main.CorePlugin;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import net.milkbowl.vault.economy.EconomyResponse.ResponseType;

public class VoxinEconomy implements Economy{
	
	private CorePlugin plugin;
	private VoxinServer type;
	
	public VoxinEconomy(CorePlugin plugin) {
		this.plugin = plugin;
		type = plugin.getConnectPlugin().getServerType();
	}

	@Override
	public boolean createPlayerAccount(OfflinePlayer arg0) {
		return plugin.getDataManager().createAccount(type, arg0.getUniqueId()) != null;
	}
	
	@Override
	public EconomyResponse depositPlayer(OfflinePlayer arg0, double arg1) {
		if(plugin.getDataManager().changeAccountBalance(type, arg0.getUniqueId(), arg1)){
			return new EconomyResponse(arg1, getBalance(arg0), ResponseType.SUCCESS, "SUCCESS");
		} else return new EconomyResponse(arg1, getBalance(arg0), ResponseType.FAILURE, "FAILURE");
	}
	
	@Override
	public double getBalance(OfflinePlayer arg0) {
		return plugin.getDataManager().getAccount(type, arg0.getUniqueId()).getBalance();
	}
	
	@Override
	public boolean has(OfflinePlayer arg0, double arg1) {
		return plugin.getDataManager().getAccount(type, arg0.getUniqueId()).getBalance() >= arg1;
	}
	
	@Override
	public boolean hasAccount(OfflinePlayer arg0) {
		return plugin.getDataManager().getAccount(type, arg0.getUniqueId()) != null;
	}
	
	@Override
	public EconomyResponse withdrawPlayer(OfflinePlayer arg0, double arg1) {
		if(plugin.getDataManager().changeAccountBalance(type, arg0.getUniqueId(), -arg1)){
			return new EconomyResponse(arg1, getBalance(arg0), ResponseType.SUCCESS, "SUCCESS");
		} else return new EconomyResponse(arg1, getBalance(arg0), ResponseType.FAILURE, "FAILURE");
	}
	
	@Override
	public boolean createPlayerAccount(String arg0) {
		return false;
	}

	@Override
	public boolean createPlayerAccount(String arg0, String arg1) {
		return false;
	}

	@Override
	public boolean createPlayerAccount(OfflinePlayer arg0, String arg1) {
		return createPlayerAccount(arg0);
	}

	@Override
	public String currencyNamePlural() {
		if(type == VoxinServer.SURVIVAL){
			return "SurvivalCoins";
		} else if(type == VoxinServer.PVE){
			return "PVECoins";
		} else return "Token";
	}

	@Override
	public String currencyNameSingular() {
		if(type == VoxinServer.SURVIVAL){
			return "SurvivalCoin";
		} else if(type == VoxinServer.PVE){
			return "PVECoin";
		} else return "Token";
	}

	@Override
	public EconomyResponse depositPlayer(String arg0, double arg1) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "Nur per OfflinePlayer möglich!");
	}

	@Override
	public EconomyResponse depositPlayer(String arg0, String arg1, double arg2) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "Nur per OfflinePlayer möglich!");
	}

	@Override
	public EconomyResponse depositPlayer(OfflinePlayer arg0, String arg1, double arg2) {
		return depositPlayer(arg0, arg2);
	}

	@Override
	public String format(double arg0) {
		return String.format("%.2f", arg0);
	}

	@Override
	public int fractionalDigits() {
		return 0;
	}

	@Override
	public double getBalance(String arg0) {
		return 0;
	}

	@Override
	public double getBalance(String arg0, String arg1) {
		return 0;
	}

	@Override
	public double getBalance(OfflinePlayer arg0, String arg1) {
		return getBalance(arg0);
	}
	
	@Override
	public String getName() {
		return "Voxin";
	}

	@Override
	public boolean has(String arg0, double arg1) {
		return false;
	}

	@Override
	public boolean has(String arg0, String arg1, double arg2) {
		return false;
	}

	@Override
	public boolean has(OfflinePlayer arg0, String arg1, double arg2) {
		return has(arg0, arg2);
	}

	@Override
	public boolean hasAccount(String arg0) {
		return false;
	}

	@Override
	public boolean hasAccount(String arg0, String arg1) {
		return false;
	}

	@Override
	public boolean hasAccount(OfflinePlayer arg0, String arg1) {
		return hasAccount(arg0);
	}
	
	@Override
	public EconomyResponse withdrawPlayer(String arg0, double arg1) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "Nur per OfflinePlayer möglich!");
	}

	@Override
	public EconomyResponse withdrawPlayer(String arg0, String arg1, double arg2) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "Nur per OfflinePlayer möglich!");
	}

	@Override
	public EconomyResponse withdrawPlayer(OfflinePlayer arg0, String arg1, double arg2) {
		return withdrawPlayer(arg0, arg2);
	}

	@Override
	public EconomyResponse deleteBank(String arg0) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VoxinCore unterstützt keine Bank!");
	}
	
	@Override
	public List<String> getBanks() {
		return Lists.newArrayList();
	}
	
	@Override
	public EconomyResponse bankBalance(String arg0) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VoxinCore unterstützt keine Bank!");
	}

	@Override
	public EconomyResponse bankDeposit(String arg0, double arg1) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VoxinCore unterstützt keine Bank!");
	}

	@Override
	public EconomyResponse bankHas(String arg0, double arg1) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VoxinCore unterstützt keine Bank!");
	}

	@Override
	public EconomyResponse bankWithdraw(String arg0, double arg1) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VoxinCore unterstützt keine Bank!");
	}

	@Override
	public EconomyResponse createBank(String arg0, String arg1) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VoxinCore unterstützt keine Bank!");
	}

	@Override
	public EconomyResponse createBank(String arg0, OfflinePlayer arg1) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VoxinCore unterstützt keine Bank!");
	}
	
	@Override
	public EconomyResponse isBankMember(String arg0, String arg1) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VoxinCore unterstützt keine Bank!");
	}

	@Override
	public EconomyResponse isBankMember(String arg0, OfflinePlayer arg1) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VoxinCore unterstützt keine Bank!");
	}

	@Override
	public EconomyResponse isBankOwner(String arg0, String arg1) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VoxinCore unterstützt keine Bank!");
	}

	@Override
	public EconomyResponse isBankOwner(String arg0, OfflinePlayer arg1) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "VoxinCore unterstützt keine Bank!");
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
	
	@Override
	public boolean hasBankSupport() {
		return false;
	}
}
