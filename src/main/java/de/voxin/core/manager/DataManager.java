package de.voxin.core.manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import de.voxin.connect.util.AccountInfo;
import de.voxin.connect.util.BanInfo;
import de.voxin.connect.util.PlayerInfo;
import de.voxin.connect.util.ServerType.VoxinServer;
import de.voxin.core.main.CorePlugin;
import de.voxin.core.mobmenu.MobMenu;
import de.voxin.core.util.Message;

public class DataManager {

	private CorePlugin plugin;
	
	private Connection connection;
	
	public DataManager(CorePlugin plugin) {
		this.plugin = plugin;
		
		connection = plugin.getConnectPlugin().getMySQLGateway().getConnection();
	}

	public PlayerInfo getPlayerInfo(String name) {

		PlayerInfo playerInfo = null;

		PreparedStatement stPlayerInfo = null;
		String queryPlayerInfo = "SELECT * FROM voxin_player WHERE name = ?";

		ResultSet rs = null;

		try {
			stPlayerInfo = connection.prepareStatement(queryPlayerInfo);
			stPlayerInfo.setString(1, name);
			rs = stPlayerInfo.executeQuery();

			while (rs.next()) {
				playerInfo = new PlayerInfo(rs.getString("name"), UUID.fromString(rs.getString("UUID")),
						rs.getTimestamp("created"), rs.getTimestamp("lastSeen"), rs.getInt("playtime"),
						rs.getString("lastIP"),
						plugin.getConnectPlugin().getConnectManager().getAllOnlinePlayerList().stream()
								.filter(s -> s.equalsIgnoreCase(name)).findAny().isPresent(),
						isBanned(UUID.fromString(rs.getString("UUID"))));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (stPlayerInfo != null && !stPlayerInfo.isClosed()) {
					stPlayerInfo.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return playerInfo;

	}

	public void banPlayer(PlayerInfo playerInfo, Date until, String reason, UUID from) {

		PreparedStatement stBan = null;
		String queryBan = new String();

		if (until == null) {
			queryBan = "INSERT INTO voxin_ban (UUID, reason, bannedOn, fromUUID, unbanned) VALUES (?, ?, ?, ?, ?)";
		} else
			queryBan = "INSERT INTO voxin_ban (UUID, reason, bannedOn, fromUUID, unbanned, until) VALUES (?, ?, ?, ?, ?, ?)";

		try {
			stBan = connection.prepareStatement(queryBan);
			stBan.setString(1, playerInfo.getUuid().toString());
			stBan.setString(2, reason);
			stBan.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
			stBan.setString(4, from.toString());
			stBan.setBoolean(5, false);
			if (until != null) {
				stBan.setTimestamp(6, new Timestamp(until.getTime()));
			}
			stBan.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stBan != null && !stBan.isClosed()) {
					stBan.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void unbanPlayer(PlayerInfo playerInfo) {

		PreparedStatement stBan = null;
		String queryBan = "UPDATE voxin_ban SET unbanned = ? WHERE UUID = ?";

		try {
			stBan = connection.prepareStatement(queryBan);
			stBan.setBoolean(1, true);
			stBan.setString(2, playerInfo.getUuid().toString());
			stBan.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stBan != null && !stBan.isClosed()) {
					stBan.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	public BanInfo isBanned(UUID uuid) {
		BanInfo banInfo = null;

		PreparedStatement stPlayerInfo = null;
		String queryPlayerInfo = "SELECT * FROM voxin_ban WHERE UUID = ? AND unbanned = ?";

		ResultSet rs = null;

		try {
			stPlayerInfo = connection.prepareStatement(queryPlayerInfo);
			stPlayerInfo.setString(1, uuid.toString());
			stPlayerInfo.setBoolean(2, false);
			rs = stPlayerInfo.executeQuery();

			while (rs.next()) {
				banInfo = new BanInfo(rs.getTimestamp("until"), rs.getString("reason"));
				if (rs.getTimestamp("until") != null && (rs.getTimestamp("until").getTime() - System.currentTimeMillis()) < 0) {
					banInfo = null;
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (stPlayerInfo != null && !stPlayerInfo.isClosed()) {
					stPlayerInfo.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return banInfo;
	}

	public AccountInfo createAccount(VoxinServer type, UUID uuid) {

		PreparedStatement st = null;
		String query = "INSERT INTO voxin_" + type.toString().toLowerCase() + "_money (UUID) VALUES (?)";

		AccountInfo account = null;

		try {
			st = connection.prepareStatement(query);
			st.setString(1, uuid.toString());
			st.executeUpdate();

			account = new AccountInfo(uuid, 0);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (st != null && !st.isClosed()) {
					st.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		return account;
	}

	public AccountInfo getAccount(VoxinServer type, UUID uuid) {

		PreparedStatement st = null;
		String query = "SELECT * FROM voxin_" + type.toString().toLowerCase() + "_money WHERE UUID = ?";
		
		ResultSet rs = null;
		AccountInfo account = null;

		try {
			st = connection.prepareStatement(query);
			st.setString(1, uuid.toString());
			rs = st.executeQuery();
			
			while(rs.next()){
				account = new AccountInfo(uuid, rs.getDouble("balance"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (st != null && !st.isClosed()) {
					st.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		return account;
	}
	
	public boolean changeAccountBalance(VoxinServer type, UUID uuid, double balance){
		
		PreparedStatement st = null;
		String query = "UPDATE voxin_" + type.toString().toLowerCase() + "_money SET balance = (balance + ?) WHERE UUID = ?";
		int changes = 0;

		try {
			st = connection.prepareStatement(query);
			st.setDouble(1, balance);
			st.setString(2, uuid.toString());
			changes = st.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (st != null && !st.isClosed()) {
					st.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		
		return changes == 1 ? true : false;
	}
	
	public Map<String, Map<String, Message>> loadMessages(){
		Map<String, Map<String, Message>> messages = Maps.newHashMap();
		
		PreparedStatement st = null;
		String query = "SELECT * FROM voxin_messages";
		
		ResultSet rs = null;

		try {
			st = connection.prepareStatement(query);
			rs = st.executeQuery();
			
			while(rs.next()){
				
				String name = rs.getString("name");
				String content = rs.getString("content");
				String language = rs.getString("language");
				
				if(!messages.containsKey(name)){
					Map<String, Message> languages = Maps.newHashMap();
					languages.put(language, new Message(name, content, language));
					messages.put(name, languages);
				} else {
					Map<String, Message> languages = messages.get(name);
					languages.put(language, new Message(name, content, language));
				}
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (st != null && !st.isClosed()) {
					st.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		
		return messages;
		
	}
	
	public ArrayList<MobMenu> loadAllMobMenus(){
		ArrayList<MobMenu> mobMenus = Lists.newArrayList();
		
		PreparedStatement st = null;
		String query = "SELECT * FROM voxin_mobmenus WHERE SERVERTYPE = ?";
		
		ResultSet rs = null;

		try {
			st = connection.prepareStatement(query);
			st.setString(1, plugin.getServerType().toString());
			rs = st.executeQuery();
			
			while(rs.next()){
				String name = rs.getString("NAME");
				EntityType type = EntityType.GIANT; 
				
				try {
					 type = EntityType.valueOf(rs.getString("ENTITYTYPE"));
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				World world = Bukkit.getWorld(rs.getString("LOC_WORLD"));
				if(world == null){
					world = Bukkit.getWorlds().get(0);
					System.out.println("WORLD NOT FOUND! ->" + rs.getString("LOC_WORLD"));
				}
				
				String cmd = rs.getString("CMD");
				
				Location loc = new Location(world, rs.getDouble("LOC_X"), rs.getDouble("LOC_Y"), rs.getDouble("LOC_Z"));
				loc.setPitch(rs.getFloat("LOC_PITCH"));
				loc.setYaw(rs.getFloat("LOC_YAW"));
				
				mobMenus.add(new MobMenu(name, type, loc, cmd));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (st != null && !st.isClosed()) {
					st.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		return mobMenus;
	}

}
