package de.voxin.core.manager;

import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.ServicePriority;

import de.voxin.connect.util.ServerType.VoxinServer;
import de.voxin.core.main.CorePlugin;
import de.voxin.core.util.VoxinEconomy;
import net.milkbowl.vault.economy.Economy;

public class VaultManager {

	private CorePlugin plugin;
	private VoxinServer type;
	
	private Economy eco;

	public VaultManager(CorePlugin plugin) {
		this.plugin = plugin;
		
		type = plugin.getConnectPlugin().getServerType();
		
		if(!(type == VoxinServer.LOBBY || type == VoxinServer.PVE || type == VoxinServer.SURVIVAL)){
			return;
		}
		
		Bukkit.getServer().getServicesManager().register(Economy.class, new VoxinEconomy(plugin), plugin, ServicePriority.Highest);
		
		setupEconomy();
	}

	private boolean setupEconomy() {
		RegisteredServiceProvider<Economy> economyProvider = plugin.getServer().getServicesManager()
				.getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (economyProvider != null) {
			eco = economyProvider.getProvider();
		}

		return (eco != null);
	}

}
