package de.voxin.core.manager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import de.voxin.core.main.CorePlugin;
import de.voxin.core.util.Permissions;
import lombok.Getter;

public class ScoreboardManager {
	
	@SuppressWarnings("unused")
	private CorePlugin plugin;
	private Scoreboard board;
	
	private @Getter NameTagColorScoreboard nameTagColorScoreboard;

	public ScoreboardManager(CorePlugin plugin) {
		this.plugin = plugin;
		
		this.nameTagColorScoreboard = new NameTagColorScoreboard();
	}

	
	public class NameTagColorScoreboard {
	
		private Team adminTeam;
		private Team modTeam;
		private Team premiumTeam;
		private Team defaultTeam;
		
		public NameTagColorScoreboard() {
			
			board = Bukkit.getScoreboardManager().getNewScoreboard();
			
			if(board.getTeam("AdminTeam") == null){
				adminTeam = board.registerNewTeam("AdminTeam");
				adminTeam.setPrefix(ChatColor.RED + "");
			} else
				adminTeam = board.getTeam("AdminTeam");
			
			if(board.getTeam("ModTeam") == null){
				modTeam = board.registerNewTeam("ModTeam");
				modTeam.setPrefix(ChatColor.DARK_AQUA + "");
			} else
				modTeam = board.getTeam("ModTeam");
			
			if(board.getTeam("PremiumTeam") == null){
				premiumTeam = board.registerNewTeam("PremiumTeam");
				premiumTeam.setPrefix(ChatColor.YELLOW + "");
			} else
				premiumTeam = board.getTeam("PremiumTeam");
			
			if(board.getTeam("DefaultTeam") == null){
				defaultTeam = board.registerNewTeam("DefaultTeam");
				defaultTeam.setPrefix(ChatColor.GRAY + "");
			} else
				defaultTeam = board.getTeam("DefaultTeam");
		
		}
		
		@SuppressWarnings("deprecation")
		public void colorNameOverPlayer(Player player){
			player.setScoreboard(board);
			if(player.hasPermission(Permissions.COLOR_ADMIN.getPermission())){
				adminTeam.addPlayer(player);
				return;
			}
			if(player.hasPermission(Permissions.COLOR_MOD.getPermission())){
				modTeam.addPlayer(player);
				return;
			}
			if(player.hasPermission(Permissions.COLOR_PREMIUM.getPermission())){
				premiumTeam.addPlayer(player);
				return;
			}
			defaultTeam.addPlayer(player);
		}
	}
}

