package de.voxin.core.manager;

import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import de.voxin.core.main.CorePlugin;
import de.voxin.core.util.Message;
import de.voxin.core.util.Permissions;

public class Localization {

	@SuppressWarnings("unused")
	private CorePlugin plugin;

	private static Map<String, Map<String, Message>> messages;

	public Localization(CorePlugin plugin) {
		this.plugin = plugin;
		messages = plugin.getDataManager().loadMessages();
	}

	public static Message getMessage(String titel, String language) {
		Message message = null;

		if (messages.containsKey(titel)) {
			if(messages.get(titel).containsKey(language)){
				message = messages.get(titel).get(language);
			} else {
				message = messages.get(titel).values().stream().findFirst().get();
			}
		} else {
			throw new RuntimeException("NO MESSAGE FOUND FOR " + titel + "!!!!");
		}
		
		return message;
	}
	
	public static void broadcast(String broadcast) {
		CorePlugin.getInstance().getConnectPlugin().getConnectManager().sendBroadcast(broadcast, "");
	}

	public static void broadcast(String broadcast, String permission) {
		CorePlugin.getInstance().getConnectPlugin().getConnectManager().sendBroadcast(broadcast, permission);
	}

	public static void sendMessage(String name, String inhalt) {
		CorePlugin.getInstance().getConnectPlugin().getConnectManager().sendMessage(name, inhalt);
	}

	public static ChatColor getColor(Player player) {
		ChatColor cc = ChatColor.GRAY;
		if (player.hasPermission(Permissions.COLOR_PREMIUM.getPermission())) {
			cc = ChatColor.YELLOW;
		}
		if (player.hasPermission(Permissions.COLOR_MOD.getPermission())) {
			cc = ChatColor.DARK_AQUA;
		}
		if (player.hasPermission(Permissions.COLOR_ADMIN.getPermission())) {
			cc = ChatColor.RED;
		}
		return cc;
	}

}
