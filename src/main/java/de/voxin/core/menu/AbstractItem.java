package de.voxin.core.menu;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import com.google.common.collect.Lists;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import de.voxin.core.main.CorePlugin;
import lombok.Getter;
import lombok.Setter;

public abstract class AbstractItem extends ItemStack {

	private @Getter @Setter UUID uuid;
	
	protected ArrayList<String> lore = Lists.newArrayList();

	public AbstractItem(Material material) {
		super(material);
		CorePlugin.getInstance().getMenuManager().addItem(this);
	}
	
	public AbstractItem(String skinURL) {
		super( Material.SKULL_ITEM, 1, (short) 3 );
		createHead(skinURL);
		CorePlugin.getInstance().getMenuManager().addItem(this);
	}

	protected void createHead(String skinURL) {

		Class<?> skullMetaClass = null;

		final String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
		try {
			skullMetaClass = Class.forName("org.bukkit.craftbukkit." + version + ".inventory.CraftMetaSkull");
		} catch (final ClassNotFoundException e) {
			e.printStackTrace();
		}

		final SkullMeta skull = (SkullMeta) this.getItemMeta();
		Field profileField = null;
		try {
			profileField = skullMetaClass.getDeclaredField("profile");
			profileField.setAccessible(true);
			profileField.set(skull, getNonPlayerProfile(skinURL));
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		}
		this.setItemMeta(skull);
	}

	private GameProfile getNonPlayerProfile(final String skinURL) {
		final GameProfile newSkinProfile = new GameProfile(UUID.randomUUID(), null);
		newSkinProfile.getProperties().put("textures",
				new Property("textures", Base64Coder.encodeString("{textures:{SKIN:{url:\"" + skinURL + "\"}}}")));
		return newSkinProfile;
	}

	public abstract void exec();

}
