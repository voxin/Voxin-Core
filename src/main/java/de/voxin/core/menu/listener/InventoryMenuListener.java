package de.voxin.core.menu.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import de.voxin.core.main.CorePlugin;
import de.voxin.core.menu.AbstractInventory;
import de.voxin.core.menu.AbstractItem;

public class InventoryMenuListener implements Listener{
	
	private CorePlugin plugin;
	
	public InventoryMenuListener( CorePlugin plugin ) {
		this.plugin = plugin;
		
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void onInventory( InventoryClickEvent event ) {
		if ( plugin.getMenuManager().containsInventory( event.getInventory() ) ) {
			
			event.setCancelled( true );
			
			Inventory inventory = event.getInventory();
			
			ItemStack clickedItem;
			try {
				clickedItem = event.getClickedInventory().getItem( event.getSlot() );
			} catch ( Exception e ) {
				return;
			}
			
			if ( plugin.getMenuManager().containsItem( clickedItem ) ) {
				AbstractItem item = plugin.getMenuManager().getItem( clickedItem );
				item.exec();
				inventory.setItem( event.getRawSlot(), item );
			}
			
		}
	}
	
	@EventHandler
	public void onInventoryClose( InventoryCloseEvent event ) {
		if ( plugin.getMenuManager().containsInventory( event.getInventory() ) ) {
			
			AbstractInventory invMenu = plugin.getMenuManager().getInventory( event.getInventory() );
			
			plugin.getMenuManager().removeItems( invMenu );
			plugin.getMenuManager().removeInventory( invMenu );
			
		}
		
	}



}
