package de.voxin.core.menu;

import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;

import com.google.common.collect.Maps;

import de.voxin.core.main.CorePlugin;
import lombok.Getter;
import lombok.Setter;

public abstract class AbstractInventory {

	private @Getter @Setter UUID uuid;

	private @Getter final String menuName;
	private @Getter final Inventory inventory;
	protected final Player player;

	private @Getter Map<AbstractItem, Integer> content = Maps.newHashMap();

	public AbstractInventory(Player player, int size, String menuName) {
		this.player = player;
		this.menuName = menuName;
		this.inventory = Bukkit.createInventory(null, size, menuName);

		loadInventory();
	}
	
	public AbstractInventory(Player player, InventoryType type, String menuName) {
		this.player = player;
		this.menuName = menuName;
		this.inventory = Bukkit.createInventory(null, type, menuName);

		loadInventory();
	}

	private void loadInventory() {
		CorePlugin.getInstance().getMenuManager().addInventory(this);
		
		Bukkit.getScheduler().runTaskLater(CorePlugin.getInstance(), () -> {
			loadInventoryContent(content);
			putItemsInInventory();
			player.openInventory(inventory);
		}, 1);
	}

	protected abstract void loadInventoryContent(Map<AbstractItem, Integer> content);

	private void putItemsInInventory() {
		content.keySet().forEach(item -> {
			if(!(content.get(item) > inventory.getSize())){
				inventory.setItem(content.get(item), item);
			}
		});
	}

}
