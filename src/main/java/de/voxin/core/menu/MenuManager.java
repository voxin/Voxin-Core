package de.voxin.core.menu;

import java.util.Map;
import java.util.UUID;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Maps;

import de.voxin.core.main.CorePlugin;
import de.voxin.core.menu.listener.InventoryMenuListener;
import lombok.Getter;

public class MenuManager {
	
	@SuppressWarnings("unused")
	private CorePlugin plugin;
	
	private @Getter Map<UUID, AbstractInventory> menuInventorys = Maps.newHashMap();
	private @Getter Map<UUID, AbstractItem> menuItems = Maps.newHashMap();
	
	public MenuManager(CorePlugin plugin) {
		this.plugin = plugin;
		
		new InventoryMenuListener(plugin);
	}
	
	public void addItem( AbstractItem menuItem ){
		UUID uuid = UUID.randomUUID();
		menuItems.put(uuid, menuItem);
		menuItem.setUuid(uuid);
	}

	public boolean containsItem(ItemStack item) {
		for ( ItemStack menuItem : menuItems.values() ) {
			if ( menuItem.equals( item ) ) {
				return true;
			}
		}
		
		return false;

	}

	public AbstractItem getItem(ItemStack item) {
		for(AbstractItem items : menuItems.values()){
			if(((ItemStack) items).equals(item)){
				return items;
			}
		}
		
		return null;
	}
	
	public void removeItem( AbstractItem button ) {
		if ( menuItems.containsValue( button ) ) {
			menuItems.remove( button.getUuid() );
		}
	}
	
	public void addInventory( AbstractInventory menuInv ){
		UUID uuid = UUID.randomUUID();
		menuInventorys.put(uuid, menuInv);
		menuInv.setUuid(uuid);
	}

	public boolean containsInventory(Inventory inventory) {
		for(AbstractInventory invs : menuInventorys.values()){
			if(invs.getInventory().equals(inventory)){
				return true;
			}
		}
		return false;
	}

	public AbstractInventory getInventory(Inventory inventory) {
		for(AbstractInventory invs : menuInventorys.values()){
			if(invs.getInventory().equals(inventory)){
				return invs;
			}
		}
		
		return null;
	}

	public void removeItems(AbstractInventory invMenu) {
		for(AbstractItem item : invMenu.getContent().keySet()){
			removeItem(item);
		}
		
	}

	public void removeInventory( AbstractInventory invMenu ) {
		if ( menuInventorys.containsValue( invMenu ) ) {
			menuInventorys.remove( invMenu.getUuid() );
		}
	}

	
	

}
