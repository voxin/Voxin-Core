package de.voxin.core.menu.inventorys;

import java.util.Map;

import org.bukkit.entity.Player;

import de.voxin.core.menu.AbstractInventory;
import de.voxin.core.menu.AbstractItem;
import de.voxin.core.menu.items.ConnectPVEServerItem;

public class NavigationMenu extends AbstractInventory{

	public NavigationMenu(Player player) {
		super(player, 27, "§6§lNavigation");
	}

	@Override
	protected void loadInventoryContent(Map<AbstractItem, Integer> content) {
		
		content.put(new ConnectPVEServerItem(player), 1);
		
	}

}
