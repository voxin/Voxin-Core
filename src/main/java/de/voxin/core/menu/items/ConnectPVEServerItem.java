package de.voxin.core.menu.items;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.meta.ItemMeta;

import de.voxin.core.main.CorePlugin;
import de.voxin.core.menu.AbstractItem;

public class ConnectPVEServerItem extends AbstractItem{

	private CorePlugin plugin;
	
	private Player player;
	
	public ConnectPVEServerItem( Player player ) {
		super(Material.GOLD_SWORD);
		
		plugin = CorePlugin.getInstance();
		
		this.player = player;
		
		ItemMeta meta = this.getItemMeta();
		meta.setDisplayName("§c§lPVE SERVER");
		
		lore.add("§eTrete dem PVE Server bei!");
		lore.add("§e0§7/§e50");
		
		meta.setLore(lore);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		
		this.setItemMeta(meta);
		this.addUnsafeEnchantment(Enchantment.LUCK, 1);
	}

	@Override
	public void exec() {
		plugin.getConnectPlugin().getConnectManager().connectPlayer(player.getUniqueId(), "pve");
	}

}
