package de.voxin.core.main;

import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;

import de.voxin.connect.main.ConnectPlugin;
import de.voxin.connect.util.ServerType.VoxinServer;
import de.voxin.core.commands.BanCommand;
import de.voxin.core.commands.ChatCommand;
import de.voxin.core.commands.MovementCommand;
import de.voxin.core.commands.PlayerCommand;
import de.voxin.core.commands.tabcompleter.VoxinCompleter;
import de.voxin.core.hologram.HologramManager;
import de.voxin.core.hologram.OnlineStaffHologram;
import de.voxin.core.listener.BoosterListener;
import de.voxin.core.listener.BuildingListener;
import de.voxin.core.listener.CommandPreProcessListener;
import de.voxin.core.listener.JoinQuitListener;
import de.voxin.core.listener.PickUpListener;
import de.voxin.core.listener.PlayerBanListener;
import de.voxin.core.listener.PlayerHealthListener;
import de.voxin.core.manager.DataManager;
import de.voxin.core.manager.Localization;
import de.voxin.core.manager.ScoreboardManager;
import de.voxin.core.manager.VaultManager;
import de.voxin.core.menu.MenuManager;
import de.voxin.core.mobmenu.MobMenuManager;
import de.voxin.core.util.PacketSending;
import lombok.Getter;

public class CorePlugin extends JavaPlugin{
	
	private static @Getter CorePlugin instance;
	
	private @Getter ConnectPlugin connectPlugin;
	private @Getter ProtocolManager protocolManager;
	
	private @Getter DataManager dataManager;
	
	private @Getter Localization localization;
	
	private @Getter VaultManager vaultManager;
	
	private @Getter VoxinServer serverType;
	
	private @Getter PacketSending packetSending;
	
	private @Getter VoxinCompleter tabCompleter;
	
	private @Getter ScoreboardManager scoreboardManager;
	private @Getter MenuManager menuManager;
	private @Getter MobMenuManager mobMenuManager;
	
	private @Getter HologramManager hologramManager;
	
	@Override
	public void onEnable() {
		instance = this;
		
		connectPlugin = ConnectPlugin.getInstance();
		serverType = connectPlugin.getServerType();
		
		dataManager = new DataManager(this);
		
		localization = new Localization(this);
		
		vaultManager = new VaultManager(this);
		
		protocolManager = ProtocolLibrary.getProtocolManager();
		
		packetSending = new PacketSending(this);
		
		tabCompleter = new VoxinCompleter(this);
		
		scoreboardManager = new ScoreboardManager(this);
		
		menuManager = new MenuManager(this);
		
		mobMenuManager = new MobMenuManager(this);
		mobMenuManager.loadAllMob();
		
		hologramManager = new HologramManager(this);
		new OnlineStaffHologram();
		
		loadCommands();
		loadListener();
	}
	
	@Override
	public void onDisable() {
		mobMenuManager.removeAllMob();
	}
	
	private void loadCommands() {
		new ChatCommand(this);
		new MovementCommand(this);
		new PlayerCommand(this);
		new BanCommand(this);
	}
	
	private void loadListener() {
		new PlayerBanListener(this);
		new BoosterListener(this);
		new PickUpListener(this);
		new BuildingListener(this);
		new PlayerHealthListener(this);
		new CommandPreProcessListener(this);
		new JoinQuitListener(this);
	}
	
	
	
}
