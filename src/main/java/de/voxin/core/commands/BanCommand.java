package de.voxin.core.commands;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.voxin.connect.util.PlayerInfo;
import de.voxin.core.main.CorePlugin;
import de.voxin.core.manager.Localization;
import de.voxin.core.util.Permissions;
import de.voxin.core.util.TimeFormatter;

public class BanCommand {

	private CorePlugin plugin;

	public BanCommand(CorePlugin plugin) {
		this.plugin = plugin;

		registerCommands();
		registerCompleter();
	}

	private void registerCommands() {
		plugin.getCommand("tempban").setExecutor(new TempBanCommand());
		plugin.getCommand("permban").setExecutor(new PermBanCommand());
		plugin.getCommand("unban").setExecutor(new UnBanCommand());
	}

	private void registerCompleter() {
		plugin.getCommand("tempban").setTabCompleter(plugin.getTabCompleter());
		plugin.getCommand("permban").setTabCompleter(plugin.getTabCompleter());
		plugin.getCommand("unban").setTabCompleter(plugin.getTabCompleter());
	}

	class TempBanCommand implements CommandExecutor {

		public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

			if (!(sender instanceof Player)) {
				Localization.getMessage("NO_CONSOLE", "en_US").sendMessage(sender);
				return true;
			}

			Player player = (Player) sender;

			if (!sender.hasPermission(Permissions.TEMPBAN.getPermission())) {
				Localization.getMessage("NO_PERMISSION", "en_US").sendMessage(player);
				return true;
			}

			if (args.length >= 3) {
				PlayerInfo playerInfo = plugin.getDataManager().getPlayerInfo(args[0]);
				if (playerInfo == null) {
					Localization.getMessage("ERROR_EXISTS", "en_US").sendMessage(player);
					return true;
				}

				String reason = new String();

				for (int i = 2; i < args.length; i++) {
					reason += args[i] + " ";
				}

				if (playerInfo.getBanInfo() != null) {
					Localization.getMessage("ERROR_BANNED", "en_US").sendMessage(player);
					return true;
				}

				long banTime = TimeFormatter.getTimeFromString(args[1]);

				if (banTime < 0) {
					Localization.getMessage("ERROR_TEMPBAN", "en_US").sendMessage(player);
					return true;
				}

				Date until = new Date(System.currentTimeMillis() + banTime);

				plugin.getDataManager().banPlayer(playerInfo, until, reason, player.getUniqueId());
				banKickPlayer(playerInfo, reason, until);
				Localization.getMessage("SUCCESS_BANNED", "en_US").replaceArgs(playerInfo.getName())
						.sendMessage(player);

			} else {
				Localization.getMessage("ERROR_TEMPBAN", "en_US").sendMessage(player);
			}

			return true;
		}

	}

	class PermBanCommand implements CommandExecutor {

		public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

			if (!(sender instanceof Player)) {
				Localization.getMessage("NO_CONSOLE", "en_US").sendMessage(sender);
				return true;
			}

			Player player = (Player) sender;

			if (!sender.hasPermission(Permissions.PERMBAN.getPermission())) {
				Localization.getMessage("NO_PERMISSION", "en_US").sendMessage(sender);
				return true;
			}

			if (args.length >= 2) {
				PlayerInfo playerInfo = plugin.getDataManager().getPlayerInfo(args[0]);
				if (playerInfo == null) {
					Localization.getMessage("ERROR_EXISTS", "en_US").sendMessage(player);
					return true;
				}

				String reason = new String();

				for (int i = 1; i < args.length; i++) {
					reason += args[i] + " ";
				}

				if (playerInfo.getBanInfo() != null) {
					Localization.getMessage("ERROR_BANNED", "en_US").sendMessage(player);
					return true;
				}

				plugin.getDataManager().banPlayer(playerInfo, null, reason, player.getUniqueId());
				banKickPlayer(playerInfo, reason, null);
				Localization.getMessage("SUCCESS_BANNED", "en_US").replaceArgs(playerInfo.getName())
						.sendMessage(player);

			} else {
				Localization.getMessage("ERROR_PERMBAN", "en_US").sendMessage(player);
			}

			return true;
		}

	}

	class UnBanCommand implements CommandExecutor {

		public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

			if (!(sender instanceof Player)) {
				Localization.getMessage("NO_CONSOLE", "en_US").sendMessage(sender);
				return true;
			}

			if (!sender.hasPermission(Permissions.UNBAN.getPermission())) {
				Localization.getMessage("NO_PERMISSION", "en_US").sendMessage(sender);
				return true;
			}

			if (args.length == 1) {
				PlayerInfo playerInfo = plugin.getDataManager().getPlayerInfo(args[0]);
				if (playerInfo == null) {
					Localization.getMessage("ERROR_EXISTS", "en_US").sendMessage(sender);
					return true;
				}

				if (playerInfo.getBanInfo() == null) {
					Localization.getMessage("ERROR_UNBANNED", "en_US").sendMessage(sender);
					return true;
				}

				plugin.getDataManager().unbanPlayer(playerInfo);
				Localization.getMessage("SUCCESS_UNBANNED", "en_US").replaceArgs(playerInfo.getName())
						.sendMessage(sender);

			} else {
				Localization.getMessage("ERROR_UNBAN", "en_US").sendMessage(sender);
			}

			return true;
		}

	}

	private void banKickPlayer(PlayerInfo playerInfo, String reason, Date until) {
		SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
		plugin.getConnectPlugin().getConnectManager().kickPlayer(playerInfo.getName(),
				Localization.getMessage("BAN_KICK", "en_US")
						.replaceArgs(reason,
								(until == null ? ChatColor.YELLOW + "NEVER" : ChatColor.YELLOW + format.format(until)))
						.getContent().replace("%n", System.lineSeparator()));
	}

}
