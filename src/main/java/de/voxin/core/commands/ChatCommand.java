package de.voxin.core.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.voxin.core.main.CorePlugin;
import de.voxin.core.manager.Localization;
import de.voxin.core.util.Permissions;
import de.voxin.core.util.Testing;

public class ChatCommand {

	private CorePlugin plugin;

	public ChatCommand(CorePlugin plugin) {
		this.plugin = plugin;

		registerCommands();
		registerCompleter();
	}

	private void registerCommands() {
		plugin.getCommand("msg").setExecutor(new MSGCommand());
		plugin.getCommand("report").setExecutor(new ReportCommand());
	}

	private void registerCompleter() {
		plugin.getCommand("msg").setTabCompleter(plugin.getTabCompleter());
	}

	class MSGCommand implements CommandExecutor {

		public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

			if (!(sender instanceof Player)) {
				Localization.getMessage("NO_CONSOLE", "en_US").sendMessage(sender);
				return true;
			}

			Player player = (Player) sender;

			if (!player.hasPermission(Permissions.PRIVATE_MESSAGING.getPermission())) {
				Localization.getMessage("NO_PERMISSION", "en_US").sendMessage(player);
				return true;
			}

			if (args.length > 1) {
				if (!Testing.testIfOnline(args[0])) {
					Localization.getMessage("ERROR_OFFLINE", "en_US").sendMessage(player);
					return false;
				}
				
				String name = args[0];
				
				for(String namen : plugin.getConnectPlugin().getConnectManager().getAllOnlinePlayerList()){
					if(namen.equalsIgnoreCase(name)){
						name = namen;
					}
				}
				
				String message = new String();

				for (int i = 1; i < args.length; i++) {
					message += args[i] + " ";
				}

				String coloredMessage = player.hasPermission(Permissions.PRIVATE_MESSAGING_COLOR.getPermission())
						? ChatColor.translateAlternateColorCodes('&', message) : message;

				Localization.sendMessage(args[0],
						ChatColor.DARK_GRAY + "[" + ChatColor.RED + ChatColor.BOLD.toString() + "MSG"
								+ ChatColor.DARK_GRAY + "] " + ChatColor.GRAY + player.getDisplayName() + ": "
								+ ChatColor.RESET + coloredMessage);
				player.sendMessage(Localization.getMessage("MSG_SENDING", "eng_US").replaceArgs(name).getContent() + coloredMessage);
			} else {
				Localization.getMessage("ERROR_MSG", "en_US").sendMessage(player);
			}

			return true;
		}

	}

	class ReportCommand implements CommandExecutor {

		public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

			if (!(sender instanceof Player)) {
				Localization.getMessage("NO_CONSOLE", "en_US").sendMessage(sender);
				return true;
			}

			Player player = (Player) sender;

			if (!player.hasPermission(Permissions.REPORT_USE.getPermission())) {
				Localization.getMessage("NO_PERMISSION", "en_US").sendMessage(player);
				return true;
			}

			if (args.length > 0) {
				String message = new String();

				for (int i = 0; i < args.length; i++) {
					message += args[i] + " ";
				}

				Localization.broadcast(ChatColor.DARK_GRAY + "[" + ChatColor.RED + ChatColor.BOLD.toString() + "REPORT"
						+ ChatColor.DARK_GRAY + "] " + ChatColor.GRAY + player.getDisplayName() + ": " + ChatColor.RESET
						+ message, Permissions.REPORT_SEE.getPermission());

				if(!player.hasPermission(Permissions.REPORT_SEE.getPermission())){
					player.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.RED + ChatColor.BOLD.toString() + "REPORT"
							+ ChatColor.DARK_GRAY + "] " + ChatColor.GRAY + player.getDisplayName() + ": " + ChatColor.RESET
							+ message);
				}
				
			} else {
				Localization.getMessage("ERROR_REPORT", "en_US").sendMessage(player);
			}

			return true;
		}

	}

}
