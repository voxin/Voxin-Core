package de.voxin.core.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.voxin.connect.util.PlayerInfo;
import de.voxin.core.main.CorePlugin;
import de.voxin.core.manager.Localization;
import de.voxin.core.util.Permissions;
import de.voxin.core.util.TimeFormatter;

public class PlayerCommand {

	private CorePlugin plugin;

	public PlayerCommand(CorePlugin plugin) {
		this.plugin = plugin;

		registerCommands();
		registerCompleter();
	}

	private void registerCommands() {
		plugin.getCommand("playerinfo").setExecutor(new InfoCommand());
	}

	private void registerCompleter() {
		plugin.getCommand("playerinfo").setTabCompleter(plugin.getTabCompleter());
	}

	class InfoCommand implements CommandExecutor {

		@Override
		public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

			if (!sender.hasPermission(Permissions.PLAYER_INFO.getPermission())) {
				Localization.getMessage("NO_PERMISSION", "en_US").sendMessage(sender);
				return true;
			}

			if (args.length == 1) {
				PlayerInfo playerInfo = plugin.getDataManager().getPlayerInfo(args[0]);
				if (playerInfo == null) {
					Localization.getMessage("ERROR_EXISTS", "en_US").sendMessage(sender);
					return true;
				}

				Localization.getMessage("PLAYERINFO_HEADER", "en_US").sendMessage(sender);

				Localization.getMessage("PLAYERINFO_ONLINE", "en_US")
						.replaceArgs(playerInfo.getName(),
								playerInfo.isOnline() ? ChatColor.GREEN + "Online ✔" : ChatColor.RED + "Offline ✘",
								(playerInfo.isOnline() ? ChatColor.GREEN : ChatColor.RED)
										+ TimeFormatter.getTageStundenSekunden(
												System.currentTimeMillis() - playerInfo.getLastSeen().getTime(), "en_US"))
						.sendMessage(sender);
				
				Localization.getMessage("PLAYERINFO_FIRST", "en_US").replaceArgs(playerInfo.getCreated().toString()).sendMessage(sender);

				Localization.getMessage("PLAYERINFO_TIME", "en_US").replaceArgs(TimeFormatter.getTageStundenSekunden(playerInfo.getTimePlayed() * 1000, "en_US")).sendMessage(sender);
				
				if (sender.hasPermission(Permissions.PLAYER_INFO_ADMIN.getPermission())) {	
					Localization.getMessage("PLAYERINFO_UUID", "en_US").replaceArgs(playerInfo.getUuid().toString()).sendMessage(sender);
					Localization.getMessage("PLAYERINFO_IP", "en_US").replaceArgs(playerInfo.getLastIP()).sendMessage(sender);
				}

			} else {
				Localization.getMessage("ERROR_INFO", "en_US").sendMessage(sender);
			}

			return true;
		}

	}

}
