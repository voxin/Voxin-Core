package de.voxin.core.commands.tabcompleter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;

import de.voxin.core.main.CorePlugin;

public class VoxinCompleter implements TabCompleter {

	private CorePlugin plugin;

	public VoxinCompleter(CorePlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			return null;
		}

		Player player = (Player) sender;

		if (cmd.getName().equalsIgnoreCase("msg") || cmd.getName().equalsIgnoreCase("tpserver")
				|| cmd.getName().equalsIgnoreCase("tempban") || cmd.getName().equalsIgnoreCase("permban")
				|| cmd.getName().equalsIgnoreCase("unban")) {
			if (args.length == 1) {
				ArrayList<String> names = Lists.newArrayList();

				if (!args[0].equals("")) {
					for (String spieler : plugin.getConnectPlugin().getConnectManager().getAllOnlinePlayerList()) {
						if (spieler.toLowerCase().startsWith(args[0].toLowerCase())) {
							names.add(spieler);
						}
					}
				} else {
					names = plugin.getConnectPlugin().getConnectManager().getAllOnlinePlayerList();
				}

				names.remove(player.getName());

				Collections.sort(names);
				return names;
			} else if (args.length > 1) {
				return Lists.newArrayList();
			}
		} else

		if (cmd.getName().equalsIgnoreCase("playerinfo")) {
			if (args.length == 1) {
				ArrayList<String> names = Lists.newArrayList();

				if (!args[0].equals("")) {
					for (String spieler : plugin.getConnectPlugin().getConnectManager().getAllOnlinePlayerList()) {
						if (spieler.toLowerCase().startsWith(args[0].toLowerCase())) {
							names.add(spieler);
						}
					}
				} else {
					names = plugin.getConnectPlugin().getConnectManager().getAllOnlinePlayerList();
				}

				Collections.sort(names);
				return names;
			} else if (args.length > 1) {
				return Lists.newArrayList();
			}
		}
		return null;
	}

}
