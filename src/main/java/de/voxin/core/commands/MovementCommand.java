package de.voxin.core.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.voxin.core.main.CorePlugin;
import de.voxin.core.manager.Localization;
import de.voxin.core.util.Permissions;
import de.voxin.core.util.Testing;

public class MovementCommand {
	
	private CorePlugin plugin;

	public MovementCommand(CorePlugin plugin) {
		this.plugin = plugin;

		registerCommands();
		registerCompleter();
	}

	private void registerCommands() {
		plugin.getCommand("tpserver").setExecutor(new TPCommand());
	}
	
	private void registerCompleter() {
		plugin.getCommand("tpserver").setTabCompleter(plugin.getTabCompleter());
	}
	
	class TPCommand implements CommandExecutor{

		@Override
		public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
			if (!(sender instanceof Player)) {
				Localization.getMessage("NO_CONSOLE", "en_US").sendMessage(sender);
				return true;
			}

			Player player = (Player) sender;

			if (!player.hasPermission(Permissions.TELEPORT_SERVER.getPermission())) {
				Localization.getMessage("NO_PERMISSION", "en_US").sendMessage(player);
				return true;
			}

			if (args.length == 1 && !args[0].equalsIgnoreCase(player.getName())) {
				if(!Testing.testIfOnline(args[0])){
					Localization.getMessage("ERROR_OFFLINE", "en_US").sendMessage(player);
					return false;
				}
				plugin.getConnectPlugin().getConnectManager().teleportPlayer(player.getName(), args[0]);
			} else {
				Localization.getMessage("ERROR_TPS", "en_US").sendMessage(player);
			}

			return true;
		}
		
	}

}
