package de.voxin.core.listener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import de.voxin.core.main.CorePlugin;
import de.voxin.core.manager.Localization;

public class JoinQuitListener implements Listener {

	private CorePlugin plugin;

	public JoinQuitListener(CorePlugin plugin) {
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
			plugin.getPacketSending().sendHeaderFooter(
					ChatColor.DARK_PURPLE + ChatColor.BOLD.toString() + "Voxin" + ChatColor.RESET + "-"
							+ ChatColor.BOLD.toString() + "NETWORK" + ChatColor.GRAY + " - " + ChatColor.YELLOW
							+ plugin.getConnectPlugin().getConnectManager().getPlayerCount("ALL") + " Spieler",
					ChatColor.GRAY + "[" + ChatColor.YELLOW + "play.voxin.net" + ChatColor.GRAY + "]" + " - "
							+ ChatColor.GREEN + ChatColor.BOLD.toString()
							+ plugin.getConnectPlugin().getServerDisplayName());
		}, 20L);
		event.getPlayer().setPlayerListName(Localization.getColor(event.getPlayer()) + event.getPlayer().getName());
		
		event.getPlayer().stopSound(Sound.BLOCK_PORTAL_AMBIENT);
		event.getPlayer().stopSound(Sound.BLOCK_PORTAL_TRAVEL);
		event.getPlayer().stopSound(Sound.BLOCK_PORTAL_TRIGGER);
		
		plugin.getScoreboardManager().getNameTagColorScoreboard().colorNameOverPlayer(event.getPlayer());

		event.setJoinMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GREEN + ChatColor.BOLD.toString() + "+"
				+ ChatColor.DARK_GRAY + "] " + ChatColor.GRAY + event.getPlayer().getName());
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
			plugin.getPacketSending().sendHeaderFooter(
					ChatColor.DARK_PURPLE + ChatColor.BOLD.toString() + "Voxin" + ChatColor.RESET + "-"
							+ ChatColor.BOLD.toString() + "NETWORK" + ChatColor.GRAY + " - " + ChatColor.YELLOW
							+ plugin.getConnectPlugin().getConnectManager().getPlayerCount("ALL") + " Spieler",
					ChatColor.GRAY + "[" + ChatColor.YELLOW + "play.voxin.net" + ChatColor.GRAY + "]" + " - "
							+ ChatColor.GREEN + ChatColor.BOLD.toString()
							+ plugin.getConnectPlugin().getServerDisplayName());
		}, 20L);
		
		event.setQuitMessage(ChatColor.DARK_GRAY + "[" + ChatColor.RED + ChatColor.BOLD.toString() + "-"
				+ ChatColor.DARK_GRAY + "] " + ChatColor.GRAY + event.getPlayer().getDisplayName());
	}

}
