package de.voxin.core.listener;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

import de.voxin.core.main.CorePlugin;

public class BoosterListener implements Listener{
	
	@SuppressWarnings("unused")
	private CorePlugin plugin;

	public BoosterListener(CorePlugin plugin) {
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void onPhysics(PlayerInteractEvent event){
		if(event.getAction() == Action.PHYSICAL){
			if(event.getClickedBlock().getType() == Material.GOLD_PLATE){
				if(event.getClickedBlock().getLocation().subtract(0, 1, 0).getBlock().getType() == Material.REDSTONE_BLOCK){
					Block block = event.getClickedBlock().getLocation().subtract(0, 2, 0).getBlock();
					if(block.getState() instanceof Sign){
						Sign sign = (Sign) block.getState();
						try {
							String[] lines = sign.getLines();
							double x = Double.valueOf(lines[0]);
							double y = Double.valueOf(lines[1]);
							double z = Double.valueOf(lines[2]);
							double force = Double.valueOf(lines[3]);
							
							Player player = event.getPlayer();
							if(x == 0 && y == 0 && z == 0 && force != 0){
								player.setVelocity(player.getLocation().getDirection().setY(0.2).multiply(force));
							} else 
							player.setVelocity(new Vector(x, y, z).multiply(force));
							
							player.playSound(player.getLocation(), Sound.BLOCK_LAVA_POP, 100, 0);
							event.setCancelled(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

}
