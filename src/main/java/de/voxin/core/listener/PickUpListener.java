package de.voxin.core.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;

import com.destroystokyo.paper.event.player.PlayerPickupExperienceEvent;

import de.voxin.connect.util.ServerType.VoxinServer;
import de.voxin.core.main.CorePlugin;
import de.voxin.core.util.Permissions;

public class PickUpListener implements Listener {

	public PickUpListener(CorePlugin plugin) {
		if (plugin.getServerType() == VoxinServer.LOBBY) {
			plugin.getServer().getPluginManager().registerEvents(this, plugin);
		}
	}

	@EventHandler
	public void onPickUp(EntityPickupItemEvent event) {
		if (!(event.getEntity() instanceof Player)) {
			return;
		}

		Player player = (Player) event.getEntity();

		if (!player.hasPermission(Permissions.PICKUP_LOBBY.getPermission())) {
			event.setCancelled(true);
			return;
		}

	}

	@EventHandler
	public void onEXPPickUp(PlayerPickupExperienceEvent event) {
		if (!event.getPlayer().hasPermission(Permissions.PICKUP_LOBBY.getPermission())) {
			event.setCancelled(true);
			return;
		}
	}

}
