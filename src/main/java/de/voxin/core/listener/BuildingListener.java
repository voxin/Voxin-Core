package de.voxin.core.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import de.voxin.connect.util.ServerType.VoxinServer;
import de.voxin.core.main.CorePlugin;
import de.voxin.core.util.Permissions;

public class BuildingListener implements Listener{
	
	public BuildingListener(CorePlugin plugin) {
		if(plugin.getServerType() == VoxinServer.LOBBY){
			plugin.getServer().getPluginManager().registerEvents(this, plugin);
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event){
		if(!event.getPlayer().hasPermission(Permissions.BUILD_LOBBY.getPermission())){
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event){
		if(!event.getPlayer().hasPermission(Permissions.BUILD_LOBBY.getPermission())){
			event.setCancelled(true);
		}
	}

}
