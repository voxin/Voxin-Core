package de.voxin.core.listener;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

import de.voxin.connect.util.BanInfo;
import de.voxin.connect.util.PlayerInfo;
import de.voxin.core.main.CorePlugin;
import de.voxin.core.manager.Localization;

public class PlayerBanListener implements Listener {

	private CorePlugin plugin;

	public PlayerBanListener(CorePlugin plugin) {
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onPreLogin(PlayerLoginEvent event) {
		PlayerInfo playerInfo = plugin.getDataManager().getPlayerInfo(event.getPlayer().getName());

		if (playerInfo.getBanInfo() == null) {
			return;
		}

		BanInfo banInfo = playerInfo.getBanInfo();
		Date bannedUntil = banInfo.getBannedUntil();
		SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

		event.setKickMessage(Localization.getMessage("BAN_KICK", "en_US").replaceArgs(banInfo.getReason(), (banInfo.getBannedUntil() == null ? ChatColor.YELLOW + "NEVER"
				: ChatColor.YELLOW + format.format(bannedUntil))).getContent().replace("%n", System.lineSeparator()));
		
		event.disallow(Result.KICK_BANNED, event.getKickMessage());
	}

}
