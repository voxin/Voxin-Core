package de.voxin.core.listener;

import java.util.Arrays;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import de.voxin.core.main.CorePlugin;
import de.voxin.core.manager.Localization;
import de.voxin.core.util.Permissions;

public class CommandPreProcessListener implements Listener {

	private String[] forbidden = { "/?", "/help", "/minecraft:help", "/bukkit:help", "/bukkit:?", "/me",
			"/minecraft:me", "/minecraft:tell", "/list", "/minecraft:list", "/trigger", "/minecraft:trigger",
			"/version", "/bukkit:version", "/plugins", "/pl", "/bukkit:plugins", "/bukkit:pl", "/bukkit:ver", 
			"/bukkit:about", "/about", "/ver", "/minecraft:w", "/w" };

	public CommandPreProcessListener(CorePlugin plugin) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onPreProcess(PlayerCommandPreprocessEvent event) {

		if (!(event.getPlayer().hasPermission(Permissions.ALL_COMMAND.getPermission()))) {

			if (Arrays.asList(forbidden).contains(event.getMessage().split(" ")[0].toLowerCase())) {
				Localization.getMessage("NO_PERMISSION", "en_US").sendMessage(event.getPlayer());
				event.setCancelled(true);
			}

		}

	}

}
