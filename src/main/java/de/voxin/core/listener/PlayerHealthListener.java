package de.voxin.core.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;

import de.voxin.connect.util.ServerType.VoxinServer;
import de.voxin.core.main.CorePlugin;

public class PlayerHealthListener implements Listener{
	
	public PlayerHealthListener(CorePlugin plugin) {
		if(plugin.getServerType() == VoxinServer.LOBBY){
			plugin.getServer().getPluginManager().registerEvents(this, plugin);
		}
	}

	@EventHandler
	public void onDamage(EntityDamageEvent event){
		if(!(event.getEntity() instanceof Player)){
			return;
		}
		
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onHunger(FoodLevelChangeEvent event){
		event.setCancelled(true);
	}
}
