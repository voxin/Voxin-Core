package de.voxin.core.mobmenu;

import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Entity;

import com.google.common.collect.Maps;

import de.voxin.core.main.CorePlugin;
import de.voxin.core.mobmenu.listener.MobMenuListener;

public class MobMenuManager {
	
	private CorePlugin plugin;
	
	private final Map<UUID, MobMenu> mobmenus = Maps.newHashMap();
	
	public MobMenuManager(CorePlugin plugin) {
		this.plugin = plugin;
		
		new MobMenuListener(plugin);
	}
	
	public void loadAllMob(){
		removeAllMob();
		plugin.getDataManager().loadAllMobMenus();
	}
	
	public void addMob(MobMenu menu){
		UUID uuid = UUID.randomUUID();
		mobmenus.put(uuid, menu);
		menu.setUuid(uuid);
	}
	
	public boolean containsMob(Entity mob){
		for(MobMenu menu : mobmenus.values()){
			if(menu.getMob().equals(mob)){
				return true;
			}
		}
		
		return false;
	}
	
	public void removeMob(MobMenu menu){
		if(mobmenus.containsValue(menu)){
			mobmenus.remove(menu.getUuid());
			menu.getMob().remove();
		}
	}
	
	public void removeAllMob(){
		for(MobMenu menu : mobmenus.values()){
			removeMob(menu);
		}
	}
	
	public MobMenu getMobMenu(Entity mob){
		for(MobMenu menu : mobmenus.values()){
			if(menu.getMob().equals(mob)){
				return menu;
			}
		}
		
		return null;
	}

}
