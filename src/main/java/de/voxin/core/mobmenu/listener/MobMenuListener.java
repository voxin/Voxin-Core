package de.voxin.core.mobmenu.listener;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;

import de.voxin.core.main.CorePlugin;
import de.voxin.core.mobmenu.MobMenu;
import de.voxin.core.util.Permissions;

public class MobMenuListener implements Listener {

	private CorePlugin plugin;

	public MobMenuListener(CorePlugin plugin) {
		this.plugin = plugin;
		
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onInteract(PlayerInteractEntityEvent event) {
		
		if(event.getHand() == EquipmentSlot.HAND){
			testAndUseEntityForMobMenu(event.getPlayer(), event.getRightClicked());
		}
		
	}

	@EventHandler
	public void onDamage(EntityDamageByEntityEvent event) {
		
		if(plugin.getMobMenuManager().containsMob(event.getEntity())){
			event.setCancelled(true);
		}

		if (!(event.getDamager() instanceof Player)) {
			return;
		}

		if (!(event.getCause() == DamageCause.ENTITY_ATTACK)) {
			return;
		}

		Player player = (Player) event.getDamager();
		if(testAndUseEntityForMobMenu(player, event.getEntity())){
		}

	}
	
	@EventHandler
	public void onFire(EntityCombustEvent event){
		if(plugin.getMobMenuManager().containsMob(event.getEntity())){
			event.setCancelled(true);
		}
	}

	private boolean testAndUseEntityForMobMenu(Player player, Entity entity) {
		if (!player.hasPermission(Permissions.USE_MOBMENU.getPermission())) {
			return false;
		}
		MobMenu menu = plugin.getMobMenuManager().getMobMenu(entity);
		if (menu == null) {
			return false;
		}

		menu.triggerMob(player);
		return true;
	}

}
