package de.voxin.core.mobmenu;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import de.voxin.core.main.CorePlugin;
import lombok.Getter;
import lombok.Setter;

public class MobMenu {
	
	private @Getter @Setter UUID uuid;
	
	private final @Getter String name;
	private final @Getter EntityType type;
	private final @Getter Location loc;
	private final String cmd;
	
	private @Getter Entity mob;
	
	public MobMenu(String name, EntityType type, Location loc, String cmd) {
		this.name = name;
		this.type = type;
		this.loc = loc;
		this.cmd = cmd;
		
		CorePlugin.getInstance().getMobMenuManager().addMob(this);
		spawnMob();
	}
	
	public void triggerMob(Player player){
		Bukkit.getServer().dispatchCommand(player, cmd);
	}
	
	private void spawnMob(){
		mob = loc.getWorld().spawnEntity(loc, type);
		mob.setCustomName(name);
		mob.setCustomNameVisible(true);
		mob.setSilent(true);
		mob.setInvulnerable(true);
		
		if(mob instanceof LivingEntity){
			LivingEntity entity = (LivingEntity) mob;
			entity.setAI(false);
			entity.setCanPickupItems(false);
			entity.setArrowsStuck(0);
		}
		
	}

}
