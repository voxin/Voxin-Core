package de.voxin.core.hologram;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.google.common.collect.Lists;

import de.voxin.core.main.CorePlugin;
import lombok.Getter;

public abstract class AbstractHologram {

	protected ArrayList<String> lines = Lists.newArrayList();

	private ArrayList<Integer> ids = Lists.newArrayList();
	private ArrayList<UUID> uuids = Lists.newArrayList();

	private final @Getter double x;
	private final @Getter double y;
	private final @Getter double z;

	private final @Getter int chunk_x;
	private final @Getter int chunk_z;

	private final @Getter World world;
	private final Location loc;

	public AbstractHologram(double x, double y, double z, World world) {
		this.x = x;
		this.y = y;
		this.z = z;

		this.world = world;

		loc = new Location(world, x, y, z);

		this.chunk_x = loc.getChunk().getX();
		this.chunk_z = loc.getChunk().getZ();
		
		CorePlugin.getInstance().getHologramManager().addHologram(this);
	}

	public void sendHologram(Player player) {

		for (int i = 0; i < lines.size(); i++) {
			PacketContainer packet = new PacketContainer(PacketType.Play.Server.SPAWN_ENTITY_LIVING);
			ids.add(i, new Random().nextInt(50000) + 500);
			packet.getIntegers().write(0, ids.get(i)).write(1, 30);

			uuids.add(i, UUID.randomUUID());
			packet.getUUIDs().write(0, uuids.get(i));
			packet.getDoubles().write(0, x).write(1, y + ((lines.size() - i) * 0.35) - 1).write(2, z);

			ArmorStand entity = (ArmorStand) world.spawnEntity(new Location(world, 0, 0, 0), EntityType.ARMOR_STAND);
			entity.setCustomNameVisible(true);
			entity.setCustomName(lines.get(i));
			entity.setGravity(false);
			entity.setVisible(false);
			WrappedDataWatcher watcher = WrappedDataWatcher.getEntityWatcher(entity).deepClone();
			entity.remove();

			packet.getDataWatcherModifier().write(0, watcher);

			try {
				CorePlugin.getInstance().getProtocolManager().sendServerPacket(player, packet);
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}

	}

	public void updateHologram(Player player) {

		updateLines();

		for (int i = 0; i < lines.size(); i++) {
			PacketContainer packet = new PacketContainer(PacketType.Play.Server.ENTITY_METADATA);

			packet.getIntegers().write(0, ids.get(i));

			ArmorStand entity = (ArmorStand) world.spawnEntity(new Location(world, 0, 0, 0), EntityType.ARMOR_STAND);
			entity.setCustomNameVisible(true);
			entity.setCustomName(lines.get(i));
			entity.setGravity(false);
			entity.setVisible(false);
			WrappedDataWatcher watcher = WrappedDataWatcher.getEntityWatcher(entity).deepClone();
			entity.remove();

			packet.getDataWatcherModifier().write(0, watcher);

			try {
				CorePlugin.getInstance().getProtocolManager().sendServerPacket(player, packet);
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}

		}

	}

	protected abstract void updateLines();

}
