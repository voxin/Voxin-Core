package de.voxin.core.hologram;

import java.util.ArrayList;

import org.bukkit.entity.Player;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.google.common.collect.Lists;

import de.voxin.core.main.CorePlugin;

public class HologramManager {
	
	private CorePlugin plugin;
	
	private final ArrayList<AbstractHologram> holograms = Lists.newArrayList();
	
	public HologramManager(CorePlugin plugin) {
		this.plugin = plugin;
		
		loadHologram();
	}
	
	public void addHologram(AbstractHologram holo){
		holograms.add(holo);
	}
	
	public void loadHologram(){
		plugin.getProtocolManager().addPacketListener(new PacketAdapter(plugin, PacketType.Play.Server.MAP_CHUNK) {
			
			@Override
			public void onPacketSending(PacketEvent event) {
				event.setReadOnly( true );
				
				PacketContainer packet = event.getPacket();
				Player player = event.getPlayer();
				
				int chunk_x = packet.getIntegers().read(0);
				int chunk_z = packet.getIntegers().read(1);
				
				for(AbstractHologram holo : holograms){
					if(holo.getChunk_x() == chunk_x && holo.getChunk_z() == chunk_z){
						holo.sendHologram(player);
					}
				}
			}
			
		});
	}

}
