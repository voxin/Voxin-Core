package de.voxin.core.hologram;

import org.bukkit.Bukkit;

import de.voxin.core.main.CorePlugin;

public class OnlineStaffHologram extends AbstractHologram {

	public OnlineStaffHologram() {
		super(-16.5, 42, -80.5, Bukkit.getWorld("main"));

		lines.add(0, "§3Player Online: §a"
				+ CorePlugin.getInstance().getConnectPlugin().getConnectManager().getAllOnlinePlayerList().size()
				+ "§7/500");
		lines.add(1, "TEST");
		lines.add(2, "TEST2");
	}

	@Override
	protected void updateLines() {
		lines.set(0, "§3Player Online: §a"
				+ CorePlugin.getInstance().getConnectPlugin().getConnectManager().getAllOnlinePlayerList().size()
				+ "§7/500");
	}

}
